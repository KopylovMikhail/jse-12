package ru.kopylov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.endpoint.IDataEndpoint;
import ru.kopylov.tm.api.endpoint.IProjectEndpoint;
import ru.kopylov.tm.api.endpoint.ITaskEndpoint;
import ru.kopylov.tm.api.endpoint.IUserEndpoint;
import ru.kopylov.tm.api.repository.*;
import ru.kopylov.tm.api.service.*;
import ru.kopylov.tm.endpoint.DataEndpoint;
import ru.kopylov.tm.endpoint.ProjectEndpoint;
import ru.kopylov.tm.endpoint.TaskEndpoint;
import ru.kopylov.tm.endpoint.UserEndpoint;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;
import ru.kopylov.tm.repository.*;
import ru.kopylov.tm.service.*;
import ru.kopylov.tm.util.DataBaseUtil;
import ru.kopylov.tm.util.HashUtil;

import javax.xml.ws.Endpoint;
import java.sql.Connection;
import java.util.Date;

@Getter
@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(this, projectRepository, taskRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(this, taskRepository);

    @NotNull
    private final IUserService userService = new UserService(this, userRepository);

    @NotNull
    private final IDataService dataService = new DataService(this);

    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionService sessionService = new SessionService(this, sessionRepository, propertyService);

    @NotNull
    public Connection getConnection() throws Exception {
        return DataBaseUtil.getConnect(propertyService.getDbHost(), propertyService.getDbLogin(), propertyService.getDbPassword());
    }

    public void init() {
        try {
            propertyService.init();
            userInit();
        }catch (Exception e) {
            e.printStackTrace();
        }
        endpointRegistry();
    }

    private void endpointRegistry() {
        @NotNull final IUserEndpoint userEndpoint = new UserEndpoint(sessionService, this);
        Endpoint.publish(userEndpoint.getUrl(), userEndpoint);
        System.out.println(userEndpoint.getUrl());
        @NotNull final IProjectEndpoint projectEndpoint = new ProjectEndpoint(sessionService, this);
        Endpoint.publish(projectEndpoint.getUrl(), projectEndpoint);
        System.out.println(projectEndpoint.getUrl());
        @NotNull final ITaskEndpoint taskEndpoint = new TaskEndpoint(sessionService, this);
        Endpoint.publish(taskEndpoint.getUrl(), taskEndpoint);
        System.out.println(taskEndpoint.getUrl());
        @NotNull final IDataEndpoint dataEndpoint = new DataEndpoint(sessionService, this);
        Endpoint.publish(dataEndpoint.getUrl(), dataEndpoint);
        System.out.println(dataEndpoint.getUrl());
    }

    private void userInit() throws Exception {
        @NotNull final User admin = new User();
        admin.setId("17909a7b-76f0-4d3f-b998-4807ce34c7bd");
        admin.setLogin("admin");
        admin.setPassword(HashUtil.hash("111111"));
        admin.setRole(TypeRole.ADMIN);
        @NotNull final User user = new User();
        user.setId("7966e8d4-1fd9-4665-aa1a-ae3a5c5f5b31");
        user.setLogin("user");
        user.setRole(TypeRole.USER);
        user.setPassword(HashUtil.hash("222222"));
        userService.persist(admin);
        userService.persist(user);

        //установка предопределенных значений для тестирования приложения
        @NotNull final Project project1 = new Project();
        @NotNull final Project project2 = new Project();
        @NotNull final Project project3 = new Project();
        project1.setName("project1");
        project2.setName("project2");
        project3.setName("project3");
        project1.setUserId(admin.getId());
        project2.setUserId(admin.getId());
        project3.setUserId(admin.getId());
        project1.setDateStart(new Date());
        project1.setDateFinish(new Date());
        project2.setDateStart(new Date());
        project2.setDateFinish(new Date());
        project3.setDateStart(new Date());
        project3.setDateFinish(new Date());
        projectService.persist(project1);
        projectService.persist(project2);
        projectService.persist(project3);
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        @NotNull final Task task3 = new Task();
        task1.setName("task1");
        task2.setName("task2");
        task3.setName("task3");
        task1.setUserId(admin.getId());
        task2.setUserId(admin.getId());
        task3.setUserId(admin.getId());
        task1.setDateStart(new Date());
        task1.setDateFinish(new Date());
        task2.setDateStart(new Date());
        task2.setDateFinish(new Date());
        task3.setDateStart(new Date());
        task3.setDateFinish(new Date());
        taskService.persist(task1);
        taskService.persist(task2);
        taskService.persist(task3);
    }

}
