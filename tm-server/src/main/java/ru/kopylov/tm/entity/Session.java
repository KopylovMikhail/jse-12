package ru.kopylov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@XmlRootElement
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Session extends AbstractEntity {

    @Nullable
    private Long timestamp = System.currentTimeMillis();

    @Nullable
    private String userId;

    @Nullable
    private String signature;

}
