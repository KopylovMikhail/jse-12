package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.AbstractEntity;
import ru.kopylov.tm.repository.AbstractRepository;

import java.util.List;

@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> {

    protected AbstractRepository<T> abstractRepository;

    protected ServiceLocator bootstrap;

    public AbstractService(@NotNull final AbstractRepository<T> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    public AbstractService(@NotNull final ServiceLocator bootstrap) {
        this.bootstrap = bootstrap;
    }

    public AbstractService(
            @NotNull final AbstractRepository<T> abstractRepository,
            @NotNull final ServiceLocator bootstrap
    ) {
        this.abstractRepository = abstractRepository;
        this.bootstrap = bootstrap;
    }

    @NotNull
    public List<T> findAll() throws Exception {
        return abstractRepository.findAll();
    }
}
