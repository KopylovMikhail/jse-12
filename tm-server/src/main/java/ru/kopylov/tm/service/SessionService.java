package ru.kopylov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.ISessionRepository;
import ru.kopylov.tm.api.service.IPropertyService;
import ru.kopylov.tm.api.service.ISessionService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.repository.SessionRepository;
import ru.kopylov.tm.util.HashUtil;
import ru.kopylov.tm.util.SignatureUtil;

import java.sql.Connection;
import java.util.List;

@Getter
@NoArgsConstructor
public final class SessionService extends AbstractService implements ISessionService {

    @NotNull
    private ISessionRepository sessionRepository = (ISessionRepository) abstractRepository;

    @NotNull
    private IPropertyService propertyService = new PropertyService();

    public SessionService(@NotNull final ISessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    public SessionService(
            @NotNull final ISessionRepository sessionRepository,
            @NotNull final IPropertyService propertyService
    ) {
        this.sessionRepository = sessionRepository;
        this.propertyService = propertyService;
    }

    public SessionService(
            @NotNull final ServiceLocator bootstrap,
            @NotNull final ISessionRepository sessionRepository,
            @NotNull final IPropertyService propertyService
    ) {
        super(bootstrap);
        this.sessionRepository = sessionRepository;
        this.propertyService = propertyService;
    }

    public boolean remove(@Nullable final String sessionId) {
        if (sessionId == null || sessionId.isEmpty()) return false;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            sessionRepository = new SessionRepository(connection);
            return sessionRepository.remove(sessionId);
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    public boolean persist(@Nullable final Session session) {
        if (session == null) return false;
        if (session.getId() == null) return false;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            sessionRepository = new SessionRepository(connection);
            return !session.equals(sessionRepository.persist(session));
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    @Nullable
    public Session persist(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        @Nullable final String sessionSignature = SignatureUtil
                .sign(session, propertyService.getSalt(), propertyService.getCycle());
        session.setSignature(sessionSignature);
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            sessionRepository = new SessionRepository(connection);
            sessionRepository.persist(session);
            return session;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    @Nullable
    public Session findOne(@Nullable final String sessionId) {
        if (sessionId == null || sessionId.isEmpty()) return null;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            sessionRepository = new SessionRepository(connection);
            return sessionRepository.findOne(sessionId);
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public void validate(@Nullable final Session clientSession) throws Exception {
        if (
                clientSession == null ||
                clientSession.getSignature() == null ||
                clientSession.getTimestamp() == null ||
                clientSession.getUserId() == null
        ) throw new Exception("User is not logged in.");
        @Nullable final Session serverSession = findOne(clientSession.getId());
        if (serverSession == null) throw new Exception("Session does not exist.");
        if (!clientSession.getSignature().equals(serverSession.getSignature()))
            throw new Exception("Session is not valid.");
        clientSession.setSignature(null);
        @Nullable final String clientSignature = SignatureUtil
                .sign(clientSession, propertyService.getSalt(), propertyService.getCycle());
        if (!serverSession.getSignature().equals(clientSignature))
            throw new Exception("Session is not valid.");
        final long timeDifference = System.currentTimeMillis() - clientSession.getTimestamp();
        if (timeDifference > propertyService.getLifeTime()) throw new Exception("Session expired.");
    }

}
