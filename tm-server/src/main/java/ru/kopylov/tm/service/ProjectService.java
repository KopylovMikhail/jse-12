package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.IProjectRepository;
import ru.kopylov.tm.api.repository.ITaskRepository;
import ru.kopylov.tm.api.service.IProjectService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.enumerated.State;
import ru.kopylov.tm.enumerated.TypeSort;
import ru.kopylov.tm.repository.ProjectRepository;
import ru.kopylov.tm.repository.TaskRepository;
import ru.kopylov.tm.util.DateUtil;
import ru.kopylov.tm.util.ProjectComparator;

import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public final class ProjectService extends AbstractService implements IProjectService {

    @NotNull
    private IProjectRepository projectRepository = (IProjectRepository) abstractRepository;

    @NotNull
    private ITaskRepository taskRepository = (ITaskRepository) abstractRepository;

    public ProjectService(
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public ProjectService(
            @NotNull final ServiceLocator bootstrap,
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository) {
        super(bootstrap);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public boolean persist(@Nullable final Project project) {
        if (project == null) return false;
        if (project.getId() == null) return false;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            projectRepository = new ProjectRepository(connection);
            projectRepository.persist(project);
            return true;
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    public boolean persist(@Nullable final String currentUserId, @Nullable final String projectName) {
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setUserId(currentUserId);
        project.setDateStart(new Date());
        project.setDateFinish(new Date());
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            projectRepository = new ProjectRepository(connection);
            return project.equals(projectRepository.persist(project));
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    @NotNull
    public List<Project> findAll() {
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            projectRepository = new ProjectRepository(connection);
            return projectRepository.findAll();
        } catch (Exception e) {
            e.getMessage();
            return Collections.emptyList();
        }
    }

    @NotNull
    public List<Project> findAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            projectRepository = new ProjectRepository(connection);
            return projectRepository.findAll(currentUserId);
        } catch (Exception e) {
            e.getMessage();
            return Collections.emptyList();
        }
    }

    @Nullable
    public List<Project> findAll(@Nullable final String currentUserId, @Nullable final String typeSort) {
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        @NotNull final List<Project> projectList;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            projectRepository = new ProjectRepository(connection);
            projectList = projectRepository.findAll(currentUserId);
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
        if (TypeSort.CREATE_DATE.getDisplayName().equals(typeSort) || typeSort == null || typeSort.isEmpty())
            return projectList;
        if (TypeSort.START_DATE.getDisplayName().equals(typeSort)) {
            projectList.sort(ProjectComparator.byDateStart);
            return projectList;
        }
        if (TypeSort.FINISH_DATE.getDisplayName().equals(typeSort)) {
            projectList.sort(ProjectComparator.byDateFinish);
            return projectList;
        }
        if (TypeSort.STATE.getDisplayName().equals(typeSort)) {
            projectList.sort(ProjectComparator.byState);
            return projectList;
        }
        return null;
    }

    public boolean merge(@Nullable final Project project) {
        if (project == null) return false;
        if (project.getId() == null || project.getId().isEmpty()) return false;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            projectRepository = new ProjectRepository(connection);
            projectRepository.merge(project);
            return true;
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    public boolean merge(
            @Nullable final String currentUserId,
            @NotNull final Integer projectNumber,
            @Nullable final String projectName,
            @Nullable final String projectDescription,
            @Nullable final String projectDateStart,
            @Nullable final String projectDateFinish,
            @Nullable final Integer stateNumber
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final List<Project> projectList;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            projectRepository = new ProjectRepository(connection);
            projectList = projectRepository.findAll(currentUserId);
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
        if (projectNumber < 1 || projectNumber > projectList.size()) return false;
        @NotNull final Project project = projectList.get(projectNumber - 1);
        if (projectName != null && !projectName.isEmpty()) project.setName(projectName);
        if (projectDescription != null && !projectDescription.isEmpty())
            project.setDescription(projectDescription);
        if (projectDateStart != null && !projectDateStart.isEmpty()) {
            @NotNull final Date dateStart = DateUtil.stringToDate(projectDateStart);
            project.setDateStart(dateStart);
        }
        if (projectDateFinish != null && !projectDateFinish.isEmpty()) {
            @NotNull final Date dateFinish = DateUtil.stringToDate(projectDateFinish);
            project.setDateFinish(dateFinish);
        }
        if (stateNumber != null) {
            if (stateNumber < 1 || stateNumber > State.values().length) return false;
            project.setState(State.values()[stateNumber-1]);
        }
        return merge(project);
    }

    public boolean remove(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return false;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            projectRepository = new ProjectRepository(connection);
            return projectRepository.remove(projectId);
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    public boolean remove(@Nullable final String currentUserId, @NotNull final Integer projectNumber) {
        @NotNull final List<Project> projectList = findAll(currentUserId);
        if (projectNumber < 1 || projectNumber > projectList.size()) return false;
        @NotNull final Project project = projectList.get(projectNumber - 1);
        return remove(project.getId());
    }

    public void removeAll() {
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            projectRepository = new ProjectRepository(connection);
            projectRepository.removeAll();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void removeAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            projectRepository = new ProjectRepository(connection);
            projectRepository.removeAll(currentUserId);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public boolean setTask(
            @Nullable final String currentUserId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) return false;
        if (taskId == null || taskId.isEmpty()) return false;
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            taskRepository = new TaskRepository(connection);
            taskRepository.setProjectId(currentUserId, projectId, taskId);
            return true;
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    public boolean setTask(
            @Nullable final String currentUserId,
            @NotNull final Integer projectNumber,
            @NotNull final Integer taskNumber
    ) {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            projectRepository = new ProjectRepository(connection);
            taskRepository = new TaskRepository(connection);
            @NotNull final List<Project> projectList = projectRepository.findAll(currentUserId);
            @NotNull final List<Task> taskList = taskRepository.findAll(currentUserId);
            if (projectNumber < 1 || projectNumber > projectList.size()) return false;
            if (taskNumber < 1 || taskNumber > taskList.size()) return false;
            @NotNull final Project project = projectList.get(projectNumber - 1);
            @NotNull final Task task = taskList.get(taskNumber - 1);
            taskRepository.setProjectId(currentUserId, project.getId(), task.getId());
            return true;
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    @NotNull
    public List<Task> getTaskList(
            @Nullable final String currentUserId,
            @Nullable final String projectId
    ) { //возвращает список задач проекта
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            taskRepository = new TaskRepository(connection);
            return taskRepository.findAllByProjectId(projectId);
        } catch (Exception e) {
            e.getMessage();
            return Collections.emptyList();
        }
    }

    @NotNull
    public List<Task> getTaskList(
            @Nullable final String currentUserId,
            @NotNull final Integer projectNumber
    ) { //возвращает список задач проекта
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        @NotNull final List<Project> projectList;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            projectRepository = new ProjectRepository(connection);
            projectList = projectRepository.findAll(currentUserId);
        } catch (Exception e) {
            e.getMessage();
            return Collections.emptyList();
        }
        if (projectNumber < 1 || projectNumber > projectList.size()) return Collections.emptyList();
        @NotNull final Project project = projectList.get(projectNumber - 1);
        return getTaskList(currentUserId, project.getId());
    }

    @NotNull
    public List<Project> findByContent(@Nullable String content) {
        if (content == null || content.isEmpty()) return Collections.emptyList();
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            projectRepository = new ProjectRepository(connection);
            return projectRepository.findByContent(content);
        } catch (Exception e) {
            e.getMessage();
            return Collections.emptyList();
        }
    }

}
