package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.ITaskRepository;
import ru.kopylov.tm.api.service.ITaskService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.enumerated.State;
import ru.kopylov.tm.enumerated.TypeSort;
import ru.kopylov.tm.repository.TaskRepository;
import ru.kopylov.tm.util.DateUtil;
import ru.kopylov.tm.util.TaskComparator;

import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public final class TaskService extends AbstractService implements ITaskService {

    @NotNull
    private ITaskRepository taskRepository = (ITaskRepository) abstractRepository;

    public TaskService(
            @NotNull final ITaskRepository taskRepository
    ) {
        this.taskRepository = taskRepository;
    }

    public TaskService(
            @NotNull final ServiceLocator bootstrap,
            @NotNull final ITaskRepository taskRepository
    ) {
        super(bootstrap);
        this.taskRepository = taskRepository;
    }

    public boolean persist(@Nullable final Task task) {
        if (task == null ) return false;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            taskRepository = new TaskRepository(connection);
            return task.equals(taskRepository.persist(task));
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    public boolean persist(@Nullable final String currentUserId, @Nullable final String taskName) {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setUserId(currentUserId);
        task.setDateStart(new Date());
        task.setDateFinish(new Date());
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            taskRepository = new TaskRepository(connection);
            return task.equals(taskRepository.persist(task));
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    @NotNull
    public List<Task> findAll() {
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            taskRepository = new TaskRepository(connection);
            return taskRepository.findAll();
        } catch (Exception e) {
            e.getMessage();
            return Collections.emptyList();
        }
    }

    @NotNull
    public List<Task> findAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            taskRepository = new TaskRepository(connection);
            return taskRepository.findAll(currentUserId);
        } catch (Exception e) {
            e.getMessage();
            return Collections.emptyList();
        }
    }

    @Nullable
    public List<Task> findAll(@Nullable final String currentUserId, @Nullable final String typeSort) {
        if (currentUserId == null || currentUserId.isEmpty()) return null;
        @NotNull final List<Task> taskList;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            taskRepository = new TaskRepository(connection);
            taskList = taskRepository.findAll(currentUserId);
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
        if (TypeSort.CREATE_DATE.getDisplayName().equals(typeSort) || typeSort == null || typeSort.isEmpty())
            return taskList;
        if (TypeSort.START_DATE.getDisplayName().equals(typeSort)) {
            taskList.sort(TaskComparator.byDateStart);
            return taskList;
        }
        if (TypeSort.FINISH_DATE.getDisplayName().equals(typeSort)) {
            taskList.sort(TaskComparator.byDateFinish);
            return taskList;
        }
        if (TypeSort.STATE.getDisplayName().equals(typeSort)) {
            taskList.sort(TaskComparator.byState);
            return taskList;
        }
        return null;
    }

    public boolean merge(@Nullable final Task task) {
        if (task == null ) return false;
        if (task.getId() == null || task.getId().isEmpty()) return false;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            taskRepository = new TaskRepository(connection);
            taskRepository.merge(task);
            return true;
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    public boolean merge(
            @Nullable final String currentUserId,
            @NotNull final Integer taskNumber,
            @Nullable final String taskName,
            @Nullable final String taskDescription,
            @Nullable final String taskDateStart,
            @Nullable final String taskDateFinish,
            @Nullable final Integer stateNumber
    ) throws Exception {
        @NotNull final List<Task> taskList;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            taskRepository = new TaskRepository(connection);
            taskList = taskRepository.findAll(currentUserId);
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
        if (taskNumber < 1 || taskNumber > taskList.size()) return false;
        @NotNull final Task task = taskList.get(taskNumber - 1);
        if (taskName != null && !taskName.isEmpty()) task.setName(taskName);
        if (taskDescription != null && !taskDescription.isEmpty())
            task.setDescription(taskDescription);
        if (taskDateStart != null && !taskDateStart.isEmpty()) {
            @NotNull final Date dateStart = DateUtil.stringToDate(taskDateStart);
            task.setDateStart(dateStart);
        }
        if (taskDateFinish != null && !taskDateFinish.isEmpty()) {
            @NotNull final Date dateFinish = DateUtil.stringToDate(taskDateFinish);
            task.setDateFinish(dateFinish);
        }
        if (stateNumber != null) {
            if (stateNumber < 1 || stateNumber > State.values().length) return false;
            task.setState(State.values()[stateNumber-1]);
        }
        return merge(task);
    }

    public boolean remove(@Nullable final String taskId) throws Exception {
        if (taskId == null || taskId.isEmpty()) return false;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            taskRepository = new TaskRepository(connection);
            return taskRepository.remove(taskId);
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    public boolean remove(@Nullable final String currentUserId, @NotNull final Integer taskNumber) {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            taskRepository = new TaskRepository(connection);
            @NotNull final List<Task> taskList = taskRepository.findAll(currentUserId);
            if (taskNumber < 1 || taskNumber > taskList.size()) return false;
            @NotNull final Task task = taskList.get(taskNumber - 1);
            return remove(task.getId());
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    public void removeAll() {
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            taskRepository = new TaskRepository(connection);
            taskRepository.removeAll();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void removeAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            taskRepository = new TaskRepository(connection);
            taskRepository.removeAll(currentUserId);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @NotNull
    public List<Task> findByContent(@Nullable String content) {
        if (content == null || content.isEmpty()) return Collections.emptyList();
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            taskRepository = new TaskRepository(connection);
            return taskRepository.findByContent(content);
        } catch (Exception e) {
            e.getMessage();
            return Collections.emptyList();
        }
    }

}
