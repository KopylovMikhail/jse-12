package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.IUserRepository;
import ru.kopylov.tm.api.service.IUserService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;
import ru.kopylov.tm.repository.UserRepository;
import ru.kopylov.tm.util.HashUtil;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

@NoArgsConstructor
public final class UserService extends AbstractService implements IUserService {

    @NotNull
    private IUserRepository userRepository = (IUserRepository) abstractRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserService(
            @NotNull final ServiceLocator bootstrap,
            @NotNull final IUserRepository userRepository
    ) {
        super(bootstrap);
        this.userRepository = userRepository;
    }

    public boolean merge(@Nullable final User user) {
        if (user == null) return false;
        if (user.getId() == null || user.getId().isEmpty()) return false;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return false;
        if (user.getPassword() == null || user.getPassword().isEmpty()) return false;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            userRepository = new UserRepository(connection);
            userRepository.merge(user);
            return true;
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    public boolean merge(
            @NotNull final String currentUserId,
            @NotNull final String login,
            @NotNull final String password
    ) throws Exception {
        @Nullable final User user = findOne(currentUserId);
        if (user == null) return false;
        @NotNull final String hashPassword = HashUtil.hash(password);
        user.setLogin(login);
        user.setPassword(hashPassword);
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            userRepository = new UserRepository(connection);
            userRepository.merge(user);
            return true;
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    public boolean persist(@Nullable final User user) {
        if (user == null) return false;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            userRepository = new UserRepository(connection);
            userRepository.persist(user);
            return true;
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    public boolean persist(@NotNull final String login, @NotNull final String password) throws Exception {
        if (password.isEmpty() || login.isEmpty()) return false;
        @NotNull final List<User> userList = findAll();
        for (@NotNull final User usr : userList) {
            if (login.equals(usr.getLogin())) return false;
        }
        @NotNull final String hashPassword = HashUtil.hash(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassword(hashPassword);
        user.setRole(TypeRole.USER);
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            userRepository = new UserRepository(connection);
            return user.equals(userRepository.persist(user));
        } catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    @Nullable
    public User findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            userRepository = new UserRepository(connection);
            return userRepository.findOne(id);
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    @Nullable
    public User findOne(@NotNull final String login, @NotNull final String password) throws Exception {
        if (password.isEmpty() || login.isEmpty()) return null;
        @NotNull final String hashPassword = HashUtil.hash(password);
        @Nullable User loggedUser = null;
        @NotNull final List<User> userList = findAll();
        for (@NotNull final User user : userList) {
            if (login.equals(user.getLogin()) && hashPassword.equals(user.getPassword())) {
                loggedUser = user;
                break;
            }
        }
        return loggedUser;
    }

    @NotNull
    public List<User> findAll() {
        try (@NotNull final Connection connection = bootstrap.getConnection()) {
            userRepository = new UserRepository(connection);
            return userRepository.findAll();
        } catch (Exception e) {
            e.getMessage();
            return Collections.emptyList();
        }
    }

}
