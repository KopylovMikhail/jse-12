package ru.kopylov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.endpoint.IProjectEndpoint;
import ru.kopylov.tm.api.service.ISessionService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.enumerated.State;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@WebService
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    private final String url = primaryUrl + this.getClass().getSimpleName() + "?wsdl";

    public ProjectEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final ServiceLocator bootstrap
    ) {
        super(sessionService, bootstrap);
    }

    @WebMethod
    public void clearProject(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        bootstrap.getProjectService().removeAll(session.getUserId());
    }

    @WebMethod
    public boolean createProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectName") @Nullable final String projectName
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        return bootstrap.getProjectService().persist(session.getUserId(), projectName);
    }

    @NotNull
    @WebMethod
    public List<Project> findProjectContent(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "findWord") @Nullable final String findWord
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        return bootstrap.getProjectService().findByContent(findWord);
    }

    @Nullable
    @WebMethod
    public List<Project> getProjectList(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "typeSort") @Nullable final String typeSort
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        return bootstrap.getProjectService().findAll(session.getUserId(), typeSort);
    }

    @WebMethod
    public boolean removeProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectNumber") @NotNull final Integer projectNumber
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        return bootstrap.getProjectService().remove(session.getUserId(), projectNumber);
    }

    @WebMethod
    public boolean setProjectTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectNumber") @NotNull final Integer projectNumber,
            @WebParam(name = "taskNumber") @NotNull final Integer taskNumber
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        return bootstrap.getProjectService().setTask(session.getUserId(), projectNumber, taskNumber);
    }

    @NotNull
    @WebMethod
    public List<Task> getProjectTaskList(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectNumber") @NotNull final Integer projectNumber
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        return bootstrap.getProjectService().getTaskList(session.getUserId(), projectNumber);
    }

    @WebMethod
    public boolean updateProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectNumber") @NotNull final Integer projectNumber,
            @WebParam(name = "projectName") @Nullable final String projectName,
            @WebParam(name = "projectDescription") @Nullable final String projectDescription,
            @WebParam(name = "projectDateStart") @Nullable final String projectDateStart,
            @WebParam(name = "projectDateFinish") @Nullable final String projectDateFinish,
            @WebParam(name = "stateNumber") @Nullable final Integer stateNumber
    ) throws Exception {
        bootstrap.getSessionService().validate(session);
        return bootstrap.getProjectService().merge(
                session.getUserId(),
                projectNumber,
                projectName,
                projectDescription,
                projectDateStart,
                projectDateFinish,
                stateNumber
        );
    }

    @NotNull
    @WebMethod
    public State[] getStateList() {
        return State.values();
    }

}
