package ru.kopylov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.endpoint.IDataEndpoint;
import ru.kopylov.tm.api.service.ISessionService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Getter
@WebService
@NoArgsConstructor
public final class DataEndpoint extends AbstractEndpoint implements IDataEndpoint {

    @NotNull
    private final String url = primaryUrl + this.getClass().getSimpleName() + "?wsdl";

    public DataEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final ServiceLocator bootstrap
    ) {
        super(sessionService, bootstrap);
    }

    @WebMethod
    public void saveDataBin(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validate(session);
        bootstrap.getDataService().saveDataBin(session.getUserId());
    }

    @WebMethod
    public void loadDataBin(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validate(session);
        bootstrap.getDataService().loadDataBin();
    }

    @WebMethod
    public void saveDataJson(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validate(session);
        bootstrap.getDataService().saveDataJson(session.getUserId());
    }

    @WebMethod
    public void loadDataJson(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validate(session);
        bootstrap.getDataService().loadDataJson();
    }

    @WebMethod
    public void saveDataJsonJaxb(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validate(session);
        bootstrap.getDataService().saveDataJsonJaxb(session.getUserId());
    }

    @WebMethod
    public void loadDataJsonJaxb(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validate(session);
        bootstrap.getDataService().loadDataJsonJaxb();
    }

    @WebMethod
    public void saveDataXml(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validate(session);
        bootstrap.getDataService().saveDataXml(session.getUserId());
    }

    @WebMethod
    public void loadDataXml(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validate(session);
        bootstrap.getDataService().loadDataXml();
    }

    @WebMethod
    public void saveDataXmlJaxb(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validate(session);
        bootstrap.getDataService().saveDataXmlJaxb(session.getUserId());
    }

    @WebMethod
    public void loadDataXmlJaxb(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        validate(session);
        bootstrap.getDataService().loadDataXmlJaxb();
    }

    private void validate(@Nullable Session session) throws Exception {
        bootstrap.getSessionService().validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable User user = bootstrap.getUserService().findOne(userId);
        if (user == null || user.getRole() != TypeRole.ADMIN) throw new Exception("User does not have enough rights.");
    }

}
