package ru.kopylov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserService {

    boolean merge(@Nullable User user) throws Exception;

    boolean persist(@Nullable User user) throws Exception;

    boolean merge(
            @NotNull String currentUserId,
            @NotNull String login,
            @NotNull String password
    ) throws Exception;

    boolean persist(@NotNull String login, @NotNull String password) throws Exception;

    @Nullable
    User findOne(@Nullable String id) throws Exception;

    @Nullable User findOne(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    List<User> findAll() throws Exception;

}
