package ru.kopylov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @NotNull
    String getUrl();

    @WebMethod
    void clearTask(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    boolean createTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "taskName") @Nullable String taskName
    ) throws Exception;

    @NotNull
    @WebMethod
    List<Task> findTaskContent(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "findWord") @Nullable String findWord
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> getTaskList(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "typeSort") @Nullable String typeSort
    ) throws Exception;

    @WebMethod
    boolean removeTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "taskNumber") @NotNull Integer taskNumber
    ) throws Exception;

    @WebMethod
    boolean updateTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "taskNumber") @NotNull Integer taskNumber,
            @WebParam(name = "taskName") @Nullable String taskName,
            @WebParam(name = "taskDescription") @Nullable String taskDescription,
            @WebParam(name = "taskDateStart") @Nullable String taskDateStart,
            @WebParam(name = "taskDateFinish") @Nullable String taskDateFinish,
            @WebParam(name = "stateNumber") @Nullable Integer stateNumber
    ) throws Exception;

}
