package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository {

    void merge(@NotNull User user) throws SQLException;

    @Nullable
    User persist(@NotNull User user) throws SQLException;

    @Nullable
    User findOne(@NotNull String id) throws Exception;

    boolean remove(@NotNull String id) throws SQLException;

    @NotNull
    List<User> findAll() throws Exception;

}
