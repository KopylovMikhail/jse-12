package ru.kopylov.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IDataService {

    void saveDataBin(@Nullable String userId) throws Exception;

    void loadDataBin() throws Exception;

    void saveDataJson(@Nullable String userId) throws Exception;

    void loadDataJson() throws Exception;

    void saveDataJsonJaxb(@Nullable String userId) throws Exception;

    void loadDataJsonJaxb() throws Exception;

    void saveDataXml(@Nullable String userId) throws Exception;

    void loadDataXml() throws Exception;

    void saveDataXmlJaxb(@Nullable String userId) throws Exception;

    void loadDataXmlJaxb() throws Exception;

}
