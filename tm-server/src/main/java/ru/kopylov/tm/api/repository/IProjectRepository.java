package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Project;

import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository {

    void merge(@NotNull Project project) throws SQLException;

    Project persist(@NotNull Project project) throws SQLException;

    @NotNull
    List<Project> findAll() throws Exception;

    @NotNull
    List<Project> findAll(@NotNull String currentUserId) throws Exception;

    boolean remove(@NotNull String projectId) throws SQLException;

    void removeAll() throws SQLException;

    void removeAll(@NotNull String currentUserId) throws SQLException;

    @Nullable
    Project findOne(@NotNull String projectId) throws Exception;

    @Nullable
    Project findOne(@NotNull String currentUserId, @NotNull String projectId) throws Exception;

    @NotNull
    List<Project> findByContent(@NotNull final String content) throws Exception;

}
