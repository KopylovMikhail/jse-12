package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Session;

import java.sql.SQLException;

public interface ISessionRepository {

    boolean remove(@NotNull String sessionId) throws SQLException;

    @Nullable
    Session persist(@NotNull final Session session) throws SQLException;

    @Nullable
    Session findOne(@NotNull final String sessionId) throws Exception;

}
