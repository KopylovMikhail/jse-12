package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository {

    void merge(@NotNull Task task) throws SQLException;

    @Nullable
    Task persist(@NotNull Task task) throws SQLException;

    @NotNull
    List<Task> findAll() throws Exception;

    @NotNull
    List<Task> findAll(@NotNull String currentUserId) throws Exception;

    boolean remove(@NotNull String taskId) throws SQLException;

    void removeAll() throws SQLException;

    void removeAll(@NotNull String currentUserId) throws SQLException;

    @Nullable
    Task findOne(@NotNull String taskId) throws Exception;

    @NotNull
    List<Task> findAllByProjectId(@NotNull final String projectId) throws Exception;

    @NotNull
    List<Task> findByContent(@NotNull final String content) throws Exception;

    void setProjectId(
            @NotNull String currentUserId,
            @NotNull String projectId,
            @NotNull String taskId
    ) throws SQLException;

}
