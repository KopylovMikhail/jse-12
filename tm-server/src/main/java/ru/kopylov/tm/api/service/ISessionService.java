package ru.kopylov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.User;

public interface ISessionService {

    boolean remove(@Nullable String sessionId) throws Exception;

    boolean persist(@Nullable Session session) throws Exception;

    @Nullable Session persist(@Nullable User user) throws Exception;

    Session findOne(@Nullable String sessionId) throws Exception;

    void validate(@Nullable final Session session) throws Exception;

}
