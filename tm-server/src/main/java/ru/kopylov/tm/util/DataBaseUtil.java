package ru.kopylov.tm.util;

import java.sql.Connection;
import java.sql.DriverManager;

public final class DataBaseUtil {

    public static Connection getConnect(String host, String login, String password) throws Exception {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        return DriverManager.getConnection(host, login, password);
    }

}
