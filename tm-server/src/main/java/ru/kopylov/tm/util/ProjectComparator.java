package ru.kopylov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.entity.Project;

import java.util.Comparator;

public final class ProjectComparator {

    @NotNull
    public final static Comparator<Project> byDateStart = new Comparator<Project>() {
        @Override
        public final int compare(@NotNull Project p1, @NotNull Project p2) {
            return p1.getDateStart().compareTo(p2.getDateStart());
        }
    };

    @NotNull
    public final static Comparator<Project> byDateFinish = new Comparator<Project>() {
        @Override
        public final int compare(@NotNull Project p1, @NotNull Project p2) {
            return p1.getDateFinish().compareTo(p2.getDateFinish());
        }
    };

    @NotNull
    public final static Comparator<Project> byState = new Comparator<Project>() {
        @Override
        public final int compare(@NotNull Project p1, @NotNull Project p2) {
            return p1.getState().compareTo(p2.getState());
        }
    };

}
