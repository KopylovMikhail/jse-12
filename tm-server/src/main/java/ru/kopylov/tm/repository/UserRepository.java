package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.IUserRepository;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository implements IUserRepository {

    public UserRepository(@Nullable Connection connection) {
        super(connection);
    }

    public void merge(@NotNull final User user) throws SQLException {
        @NotNull final String query = "UPDATE taskmanager.app_user SET login = ?, passwordHash = ?, role = ? WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, user.getLogin());
        preparedStatement.setString(2, user.getPassword());
        preparedStatement.setString(3, user.getRole().toString());
        preparedStatement.setString(4, user.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Nullable
    public User persist(@NotNull final User user) throws SQLException {
        @NotNull final String query = "INSERT INTO taskmanager.app_user (id, login, passwordHash, role) VALUES (?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, user.getId());
        preparedStatement.setString(2, user.getLogin());
        preparedStatement.setString(3, user.getPassword());
        preparedStatement.setString(4, user.getRole().toString());
        final int result = preparedStatement.executeUpdate();
        preparedStatement.close();
        if (result < 1) return null;
        return user;
    }

    @Nullable
    public User findOne(@NotNull final String id) throws Exception {
        @NotNull final String query = "SELECT * FROM taskmanager.app_user WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final User user = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return user;
    }

    public boolean remove(@NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM taskmanager.app_user WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        final int result = preparedStatement.executeUpdate();
        preparedStatement.close();
        return result > 0;
    }

    @NotNull
    public List<User> findAll() throws Exception {
        @NotNull final String query = "SELECT * FROM taskmanager.app_user";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<User> users = new ArrayList<>();
        while (resultSet.next()) users.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return users;
    }

    @Nullable
    private User fetch(@Nullable final ResultSet row) throws Exception {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPassword(row.getString("passwordHash"));
        user.setRole(TypeRole.valueOf(row.getString("role")));
        return user;
    }

}
