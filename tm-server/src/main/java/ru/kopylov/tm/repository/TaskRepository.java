package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.ITaskRepository;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.enumerated.State;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository implements ITaskRepository {

    public TaskRepository(@Nullable final Connection connection) {
        super(connection);
    }

    public void merge(@NotNull final Task task) throws SQLException {
        @NotNull final String query = "UPDATE taskmanager.app_task SET name = ?, description = ? " +
                ", dateStart = ?, dateFinish = ?, project_id = ?, user_id = ?, state = ? WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, task.getName());
        preparedStatement.setString(2, task.getDescription());
        preparedStatement.setDate(3, new Date(task.getDateStart().getTime()));
        preparedStatement.setDate(4, new Date(task.getDateFinish().getTime()));
        preparedStatement.setString(5, task.getProjectId());
        preparedStatement.setString(6, task.getUserId());
        preparedStatement.setString(7, task.getState().toString());
        preparedStatement.setString(8, task.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Nullable
    public Task persist(@NotNull final Task task) throws SQLException {
        @NotNull final String query = "INSERT INTO taskmanager.app_task " +
                "(id, name, description, dateStart, dateFinish, project_id, user_id, state) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, task.getId());
        preparedStatement.setString(2, task.getName());
        preparedStatement.setString(3, task.getDescription());
        preparedStatement.setDate(4, new Date(task.getDateStart().getTime()));
        preparedStatement.setDate(5, new Date(task.getDateFinish().getTime()));
        preparedStatement.setString(6, task.getProjectId());
        preparedStatement.setString(7, task.getUserId());
        preparedStatement.setString(8, task.getState().toString());
        final int result = preparedStatement.executeUpdate();
        preparedStatement.close();
        if (result < 1) return null;
        return task;
    }

    @NotNull
    public List<Task> findAll() throws Exception {
        @NotNull final String query = "SELECT * FROM taskmanager.app_task";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return tasks;
    }

    @NotNull
    public List<Task> findAll(@NotNull final String currentUserId) throws Exception {
        @NotNull final String query = "SELECT * FROM taskmanager.app_task WHERE user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, currentUserId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        connection.close();
        return tasks;
    }

    public boolean remove(@NotNull final String taskId) throws SQLException {
        @NotNull final String query = "DELETE FROM taskmanager.app_task WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, taskId);
        final int result = preparedStatement.executeUpdate();
        preparedStatement.close();
        return result > 0;
    }

    public void removeAll() throws SQLException {
        @NotNull final String query = "DELETE FROM taskmanager.app_task";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public void removeAll(@NotNull final String currentUserId) throws SQLException {
        @NotNull final String query = "DELETE FROM taskmanager.app_task WHERE user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, currentUserId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Nullable
    public Task findOne(@NotNull final String taskId) throws Exception {
        @NotNull final String query = "SELECT * FROM taskmanager.app_task WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, taskId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Task task = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return task;
    }

    @NotNull
    public List<Task> findAllByProjectId(@NotNull final String projectId) throws Exception {
        @NotNull final String query = "SELECT * FROM taskmanager.app_task WHERE project_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return tasks;
    }

    @NotNull
    public List<Task> findByContent(@NotNull final String content) throws Exception {
        @NotNull final String query = "SELECT * FROM taskmanager.app_task " +
                "WHERE name LIKE '%" + content + "%' OR description LIKE '%" + content + "%'";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return tasks;
    }

    public void setProjectId(
            @NotNull final String currentUserId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws SQLException {
        @NotNull final String query = "UPDATE taskmanager.app_task SET project_id = ? " +
                "WHERE id = ? AND user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, projectId);
        preparedStatement.setString(2, taskId);
        preparedStatement.setString(3, currentUserId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Nullable
    private Task fetch(@Nullable final ResultSet row) throws Exception {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setDateStart(row.getDate("dateStart"));
        task.setDateFinish(row.getDate("dateFinish"));
        task.setProjectId(row.getString("project_id"));
        task.setUserId(row.getString("user_id"));
        task.setState(State.valueOf(row.getString("state")));
        return task;
    }

}
