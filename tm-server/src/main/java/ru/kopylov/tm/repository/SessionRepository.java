package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.ISessionRepository;
import ru.kopylov.tm.entity.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@NoArgsConstructor
public final class SessionRepository extends AbstractRepository implements ISessionRepository {

    public SessionRepository(@Nullable final Connection connection) {
        super(connection);
    }

    @Override
    public boolean remove(@NotNull final String sessionId) throws SQLException {
        @NotNull final String query = "DELETE FROM taskmanager.app_session WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, sessionId);
        final int result = preparedStatement.executeUpdate();
        preparedStatement.close();
        return result > 0;
    }

    @Override
    @Nullable
    public Session persist(@NotNull final Session session) throws SQLException {
        @NotNull final String query = "INSERT INTO taskmanager.app_session " +
                "(id, timestamp , user_id, signature) VALUES (?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, session.getId());
        preparedStatement.setLong(2, session.getTimestamp());
        preparedStatement.setString(3, session.getUserId());
        preparedStatement.setString(4, session.getSignature());
        final int result = preparedStatement.executeUpdate();
        preparedStatement.close();
        if (result < 1) return null;
        return session;
    }

    @Nullable
    public Session findOne(@NotNull final String sessionId) throws Exception {
        @NotNull final String query = "SELECT * FROM taskmanager.app_session WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, sessionId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Session session = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return session;
    }

    @Nullable
    private Session fetch(@Nullable final ResultSet row) throws Exception {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setTimestamp(row.getLong("timestamp"));
        session.setUserId(row.getString("user_id"));
        session.setSignature(row.getString("signature"));
        return session;
    }

}
