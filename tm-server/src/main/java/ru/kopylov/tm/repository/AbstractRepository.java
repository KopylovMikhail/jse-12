package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.AbstractEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
public abstract class AbstractRepository<T extends AbstractEntity> {

    @Nullable
    protected Connection connection;

    public AbstractRepository(@Nullable final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected final Map<String, T> entityMap = new LinkedHashMap<>();

    public void merge(@NotNull final T entity) {
        entityMap.put(entity.getId(), entity);
    }

    @Nullable
    T persist(@NotNull final T entity) {
        return entityMap.putIfAbsent(entity.getId(), entity);
    }

    @NotNull
    public List<T> findAll() throws Exception {
        return new ArrayList<>(entityMap.values());
    }

    public abstract boolean remove(@NotNull final String entityId) throws SQLException;

    public void removeAll() throws SQLException {
        entityMap.clear();
    }

}
