package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.IProjectRepository;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.enumerated.State;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository implements IProjectRepository {

    public ProjectRepository(@Nullable final Connection connection) {
        super(connection);
    }

    public void merge(@NotNull final Project project) throws SQLException {
        @NotNull final String query = "UPDATE taskmanager.app_project SET name = ?, description = ? " +
                ", dateStart = ?, dateFinish = ?, user_id = ?, state = ? WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, project.getName());
        preparedStatement.setString(2, project.getDescription());
        preparedStatement.setDate(3, new Date(project.getDateStart().getTime()));
        preparedStatement.setDate(4, new Date(project.getDateFinish().getTime()));
        preparedStatement.setString(5, project.getUserId());
        preparedStatement.setString(6, project.getState().toString());
        preparedStatement.setString(7, project.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Nullable
    public Project persist(@NotNull final Project project) throws SQLException {
        @NotNull final String query = "INSERT INTO taskmanager.app_project " +
                "(id, name, description, dateStart, dateFinish, user_id, state) VALUES (?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, project.getId());
        preparedStatement.setString(2, project.getName());
        preparedStatement.setString(3, project.getDescription());
        preparedStatement.setDate(4, new Date(project.getDateStart().getTime()));
        preparedStatement.setDate(5, new Date(project.getDateFinish().getTime()));
        preparedStatement.setString(6, project.getUserId());
        preparedStatement.setString(7, project.getState().toString());
        final int result = preparedStatement.executeUpdate();
        preparedStatement.close();
        if (result < 1) return null;
        return project;
    }

    @NotNull
    public List<Project> findAll() throws Exception {
        @NotNull final String query = "SELECT * FROM taskmanager.app_project";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) projects.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return projects;
    }

    @NotNull
    public List<Project> findAll(@NotNull final String currentUserId) throws Exception {
        @NotNull final String query = "SELECT * FROM taskmanager.app_project WHERE user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, currentUserId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) projects.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return projects;
    }

    public boolean remove(@NotNull final String projectId) throws SQLException {
        @NotNull final String query = "DELETE FROM taskmanager.app_project WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, projectId);
        final int result = preparedStatement.executeUpdate();
        preparedStatement.close();
        return result > 0;
    }

    public void removeAll() throws SQLException {
        @NotNull final String query = "DELETE FROM taskmanager.app_project";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    public void removeAll(@NotNull final String currentUserId) throws SQLException {
        @NotNull final String query = "DELETE FROM taskmanager.app_project WHERE user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, currentUserId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Nullable
    public Project findOne(@NotNull final String projectId) throws Exception {
        @NotNull final String query = "SELECT * FROM taskmanager.app_project WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Project project = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return project;
    }

    @Nullable
    public Project findOne(@NotNull final String currentUserId, @NotNull final String projectId) throws Exception {
        @NotNull final String query = "SELECT * FROM taskmanager.app_project WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, currentUserId);
        preparedStatement.setString(2, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Project project = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return project;
    }

    @NotNull
    public List<Project> findByContent(@NotNull final String content) throws Exception {
        @NotNull final String query = "SELECT * FROM taskmanager.app_project " +
                "WHERE name LIKE '%" + content + "%' OR description LIKE '%" + content + "%'";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) projects.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return projects;
    }

    @Nullable
    private Project fetch(@Nullable final ResultSet row) throws Exception {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setDateStart(row.getDate("dateStart"));
        project.setDateFinish(row.getDate("dateFinish"));
        project.setUserId(row.getString("user_id"));
        project.setState(State.valueOf(row.getString("state")));
        return project;
    }

}
