package ru.kopylov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.context.Bootstrap;

@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    protected Bootstrap bootstrap = new Bootstrap();

    public AbstractCommand(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void setBootstrap(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

}
